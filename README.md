# Koji Operator

[![Documentation Status](https://readthedocs.org/projects/koji-operator/badge/?version=latest)](https://koji-operator.readthedocs.io/en/latest/?badge=latest)

Koji Kubernetes Operator - manage and run [Koji](https://pagure.io/koji/) in a Kubernetes cluster.

## Development

### Requirements

Development requires the following tooling:

* operator-sdk >= 1.7;
* ansible >= 2.9;
* molecule >= 3.2 (for running tests);
* kustomize >= 4.2 (for running tests);
* A container runtime such as docker;
* Minikube.

### Testing

Molecule is the tool of choice when it comes to running operator e2e tests.

The operator can be tested locally by running: `molecule test -s minikube`.

All tests will run in a minikube instance (vm, containers, etc).

## License

MIT (see LICENSE file)
