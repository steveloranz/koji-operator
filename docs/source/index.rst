.. Koji Kuberetes Operator documentation master file, created by
   sphinx-quickstart on Fri Sep  3 17:41:15 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Koji Kuberetes Operator's documentation!
===================================================

.. toctree::
   :caption: Contents:
  
   introduction
   community



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
