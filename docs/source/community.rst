Community
=========

Communication
-------------

+----------------+-----------------------------------------------------------+
|                | Channels                                                  |
+================+===========================================================+
| Repository     | https://pagure.io/kube-sig/koji-operator                  |
+----------------+-----------------------------------------------------------+
| Wiki           | `SIGs/KubeDev`_                                           |
+----------------+-----------------------------------------------------------+
| IRC            | `#fedora-kube`_ channel in the `libera.chat`_ network.    |
+----------------+-----------------------------------------------------------+
| Mailing List   | `fedora-kube`_ (`subscribe`_)                             |
+----------------+-----------------------------------------------------------+

.. _SIGs/KubeDev: https://fedoraproject.org/wiki/SIGs/KubeDev
.. _#fedora-kube: https://web.libera.chat/#fedora-kube
.. _libera.chat: http://libera.chat/
.. _fedora-kube: https://lists.fedoraproject.org/archives/list/kube@lists.fedoraproject.org/
.. _subscribe: https://lists.fedoraproject.org/admin/lists/kube.lists.fedoraproject.org/

