#!/bin/bash

# status server
mkdir -p /opt/cloud-init-status-server
ln -sf /var/lib/cloud/data/result.json /opt/cloud-init-status-server/result.json
cp /mnt/cloud-init-status-server/cloud-init-status-server.service /usr/lib/systemd/system/cloud-init-status-server.service
systemctl enable cloud-init-status-server
systemctl start cloud-init-status-server
 
# packages
dnf install -y --setopt='fastestmirror=True' koji-builder python3-gunicorn python3-flask

#kojid setup
mkdir /mnt/kojid
rm -f /etc/kojid/kojid.conf
ln -sf /mnt/kojid/kojid.conf /etc/kojid/kojid.conf
 
# mock setup
mkdir /mnt/mockcfg
rm -f /etc/mock/site-defaults.cfg
ln -sf /mnt/mockfg/site-defaults.cfg /etc/mock/site-defaults.cfg
usermod -aG mock kojibuilder

# cert setup
mkdir -p /etc/certs/ca /etc/certs/client
chown -R kojibuilder /etc/certs
ln -sf /mnt/ca-cert/cert /etc/certs/ca/cert
ln -sf /mnt/client-cert/client.pem /etc/certs/client/client.pem
ln -sf /mnt/ca-cert/cert /etc/pki/ca-trust/source/anchors/ca.pem
ln -sf /mnt/client-cert/client.pem /etc/pki/ca-trust/source/anchors/client.pem
update-ca-trust 

# kojid system enable
systemctl enable kojid
systemctl start kojid
