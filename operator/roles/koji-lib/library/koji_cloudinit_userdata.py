from ansible.module_utils.basic import AnsibleModule


def main():
    output = {
        'changed': False,
        'failed': False,
        'skipped': False,
        'msg': '',
        'data': ''
    }

    module_args = dict(
        user_config=dict(type='str', required=True),
        user_script=dict(type='str', required=True)
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    user_config = module.params['user_config']
    user_script = module.params['user_script']

    res = module.run_command([
        'cloud-init',
        'devel', 'make-mime',
        '-a', f'{user_config}:cloud-config',
        '-a', f'{user_script}:x-shellscript'
    ])

    if res[0] != 0:
        output['failed'] = True
        output['msg'] = 'cloud-init failed to run'
        output['data'] = res[1]
        module.fail_json(**output)

    output['changed'] = True
    output['msg'] = 'Multi part file content generated'
    output['data'] = res[1]
    module.exit_json(**output)


if __name__ == '__main__':
    main()
