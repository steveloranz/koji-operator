# This file is part of the koji-operator project.
# Copyright (C) 2020  Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

DOCUMENTATION = '''
---
module: koji_client_cert

short_description: Ansible module that creates a client certificate for koji access.

description:
  - This is an action plugin that uses several ansible modules (openssl + kubernetes)
  - This module loads the root CA from kubernetes and creates signs a new certificate from it
  - It uses the username as the certfificate common name
  - It creates the a kubernetes secret with both the private key and certificate in a single file (client.pem)
  - It returns both client.pem content and the kubernetes secret resource

options:
    k8s_ca:
        description:
            - the kubernetes ca secret namespace and name ($NAMESPACE/$NAME)
        required: true
    k8s_secret:
        description:
            - the kubernetes client secret namespace and name ($NAMESPACE/$NAME) to be created
            - it will skip the module run if a secret already exists and 'force' is set to false
    k8s_meta:
        description:
            - A dicitonary indicating any extra fields to add into the secret "metadata" data;
            - labels, annotations, etc.
        required: false
    username:
        description:
            - the common name to be used in the certificate (it needs to be the same username used in koji)
        required: true
    force:
      description:
        - A boolean flag that will re-create the kubernetes client secret in case one was already created with the same name
      default: false

author:
    - Red Hat, Inc. and others
'''

EXAMPLES = '''
- koji_client_cert:
    k8s_ca: koji/koji-hub-ca
    k8s_cert: "koji/koji-web-client-cert
    k8s_meta:
      labels:
        app: myapp
      annotations:
        a: 2
    username: kojiweb
    force: false
'''


from pathlib import Path
from base64 import b64encode, b64decode

import yaml
from ansible.plugins.action import ActionBase


class ActionModule(ActionBase):
    def create_subdirs(self):
        """Create directories in the run tmp folder"""
        tmpdir = self._connection._shell.tmpdir
        Path(f'{tmpdir}/certs/ca').mkdir(parents=True, exist_ok=True)
        Path(f'{tmpdir}/certs/client').mkdir(parents=True, exist_ok=True)
        Path(f'{tmpdir}/k8s').mkdir(parents=True, exist_ok=True)

    def file_write(self, path, data):
        """Helper function to write a file"""
        with open(path, 'w+') as f:
            f.write(str(data))

    def file_read(self, path):
        """Helper function to read from a file"""
        with open(path, 'r') as f:
            return f.read()

    def run_setup(self, task_vars=None):
        """Runs the setup module"""
        self._execute_module(
            'setup',
            module_args=dict(
                gather_subset='all',
                gather_timeout=10
            ),
            task_vars=task_vars
        )
    def run_k8s_info(self, api_version, kind, ref, task_vars=None):
        """Runs the k8s_info module"""
        namespace, name = ref.split('/')
        
        res = self._execute_module(
            'kubernetes.core.k8s',
            module_args=dict(
                api_version=api_version,
                kind=kind,
                namespace=namespace,
                name=name),
            task_vars=task_vars)

        if res.get('failed'):
            return None

        if len(res.get('result', {}).get('data', {})) == 0:
            return None
        
        return res.get('result')

    def run_private_key(self, basedir, key_size=4096, task_vars=None):
        """Runs the openssl_privatekey module"""
        return self._execute_module(
            'ansible.builtin.openssl_privatekey',
            module_args=dict(
                path=f'{basedir}/certs/client/key.pem',
                size=key_size
            ),
            task_vars=task_vars
        )

    def run_openssl_csr(self, basedir, private_key, common_name, task_vars=None):
        return self._execute_module(
            'ansible.builtin.openssl_csr',
            module_args=dict(
                path=f'{basedir}/certs/client/request.csr',
                privatekey_path=private_key,
                common_name=common_name
            ),
            task_vars=task_vars
        )

    def run_openssl_certificate(self, basedir, task_vars=None):
        return self._execute_module(
            'ansible.builtin.openssl_certificate',
            module_args=dict(
                path=f'{basedir}/certs/client/cert.pem',
                csr_path=f'{basedir}/certs/client/request.csr',
                ownca_path=f'{basedir}/certs/ca/cert.pem',
                ownca_privatekey_path=f'{basedir}/certs/ca/key.pem',
                provider='ownca'
            ),
            task_vars=task_vars
        )

    def run_k8s(self, data, ref, task_vars=None):
        namespace, name = ref.split('/')

        return self._execute_module(
            'kubernetes.core.k8s',
            module_args=dict(
                state='present',
                definition=data
            ),
            task_vars=task_vars
        )

    def run(self, tmp=None, task_vars=None):
        super(ActionModule, self).run(tmp, task_vars)
        
        output = {
            'failed': False,
            'changed': False,
            'skipped': False,
            'msg': '',
            'result': {}
        }

        # plugin params
        module_args = self._task.args.copy()

        param_k8s_ca = module_args.get('k8s_ca')
        param_k8s_cert = module_args.get('k8s_cert')
        param_username = module_args.get('username')
        param_force = module_args.get('force', False)
        param_k8s_meta = module_args.get('k8s_meta', {})

        # run setup module so facts are gathered
        self.run_setup(task_vars)
        self.create_subdirs()
        tmpdir = self._connection._shell.tmpdir

        # retrieves an existing client cert (if any)
        # skips the run in case a secret is found
        # it won't skip if "force" is set to True
        k8s_cert = self.run_k8s_info('v1', 'Secret', param_k8s_cert, task_vars=task_vars)
        if k8s_cert is not None and not param_force:
            output['skipped'] = True
            output['msg'] = f'Secret {param_k8s_cert} already exists'
            return output

        #  retrieves the root ca secret from kubernetes
        #  fails if no secret is found
        # fails if secret does not have the expected format
        k8s_ca = self.run_k8s_info('v1', 'Secret', param_k8s_ca, task_vars=task_vars)
        if k8s_ca is None:
            output['failed'] = True
            output['msg'] = f'Secret {param_k8s_ca}  not found'
            return output
        if k8s_ca.get('data', {}).get('cert', None) is None:
            output['failed'] = True
            output['msg'] = f'Secret {param_k8s_ca} cannot be parsed'
            return output

        # writes ca cert and key to tmp task folder
        ca_cert = self.file_write(f'{tmpdir}/certs/ca/cert.pem', b64decode(k8s_ca['data']['cert']).decode())
        ca_key = self.file_write(f'{tmpdir}/certs/ca/key.pem', b64decode(k8s_ca['data']['key']).decode())

        # creates the client private key
        private_key = self.run_private_key(
            f'{tmpdir}',
            task_vars=task_vars
        )
        if private_key.get('failed'):
            output['failed'] = True
            output['msg'] = private_key['msg']
            return output

        # create client csr
        csr = self.run_openssl_csr(
            f'{tmpdir}',
            private_key['filename'],
            param_username,
            task_vars=task_vars)
        if csr.get('failed'):
            output['failed'] = True
            output['msg'] = csr['msg']
            return output
        
        # create client certificate
        cert = self.run_openssl_certificate(tmpdir, task_vars=task_vars)
        if cert.get('failed'):
            output['failed'] = True
            output['msg'] = cert['msg']
            return output

        # create k8s cert secret
        client_pem = self.file_read(private_key['filename']) + self.file_read(cert['filename'])
        k8s_cert_namespace, k8s_cert_name = param_k8s_cert.split('/')
        k8s_secret_res = {
            'apiVersion': 'v1',
            'kind': 'Secret',
            'metadata': {
                'name': k8s_cert_name,
                'namespace': k8s_cert_namespace,
                'labels': {
                    'app': 'koji'
                }
            },
            'data': {
                'client.pem': b64encode(client_pem.encode()).decode()
             }
        }
        for k,v in param_k8s_meta.items():
            k8s_secret_res['metadata'][k] = v
        
        k8s_client_secret = self.run_k8s(k8s_secret_res, param_k8s_cert, task_vars=task_vars)
        
        # set response dict
        output['changed'] = True
        output['msg'] = f'Secret {param_k8s_cert} created'
        output['result'] = {
            'pemfile': client_pem,
            'k8s': k8s_client_secret['result']
        }
        
        return output

