from ansible.plugins.action import ActionBase

class ActionModule(ActionBase):
    
    def _run_setup(self, task_vars=None):
        """Runs the setup module"""
        self._execute_module(
            'setup',
            module_args=dict(
                gather_subset='all',
                gather_timeout=10
            ),
            task_vars=task_vars
        )

    def _read_k8s_kojihub(self, name, namespace, task_vars=None):
        """runs the k8s module to read a KojiHub resource"""
        vg = 'buildsys.apps.fedoraproject.org/v1alpha1'
        kind = 'KojiHub'

        res = self._execute_module(
            'kubernetes.core.k8s',
            module_args=dict(
                api_version=vg,
                kind=kind,
                namespace=namespace,
                name=name),
            task_vars=task_vars)

        if res.get('failed'):
            return None

        if len(res.get('result', {}).get('spec', {})) == 0:
            return None
        
        return res.get('result')

    def _read_hub_svc(self, name, namespace, task_vars=None):
        """runs the k8s module to read a Service resource"""
        res = self._execute_module(
            'kubernetes.core.k8s',
            module_args=dict(
                api_version='v1',
                kind='Service',
                namespace=namespace,
                name=name),
            task_vars=task_vars)

        if res.get('failed'):
            return None

        if len(res.get('result', {}).get('spec', {})) == 0:
            return None

        res = res.get('result')
        if not res:
            return None

        try:
            ports = res.get('spec', {}).get('ports', [])
            https_port = [p for p in ports if p.get('name') == 'https'][0]['port']
        except IndexError:
            return None

        return f'{name}.{namespace}.svc.cluster.local:{https_port}'


    def run(self, tmp=None, task_vars=None):
        super(ActionModule, self).run(tmp, task_vars)
        
        # run setup module so facts are gathered
        self._run_setup(task_vars)
        
        output = {
            'failed': False,
            'changed': False,
            'skipped': False,
            'msg': '',
            'result': {}
        }

        # plugin params
        module_args = self._task.args.copy()
        param_hub = module_args.get('hub', '')

        try:
            name, namespace = param_hub.split('/')
        except ValueError:
            output['failed'] = True
            output['msg'] = f'Failed to parse hub reference: {param_hub}'
            return output

        hub = self._read_k8s_kojihub(name, namespace, task_vars=task_vars)
        if hub is None:
            output['failed'] = True
            output['message'] = f'Failed to read hub info: {param_hub}'
            return output
        
        svc_name = hub['spec']['svc_name']
        svc = self._read_hub_svc(svc_name, namespace, task_vars=task_vars)
        if svc is None:
            output['failed'] = True
            output['message'] = f'Failed to read svc info: {svc_name}'
            return output

        output['changed'] = True
        output['msg'] = 'KojiHub info retrieved'
        output['result'] = {
            'hub': hub,
            'svc': svc
        }
        
        return output 
